var textStyleCache_airconclimatedata = {}
var clusterStyleCache_airconclimatedata = {}
var style_airconclimatedata = function(feature, resolution) {
    var size = feature.get('features').length;
    if (size != 1) {
        var style = clusterStyleCache_airconclimatedata[size];
        if (!style) {
            style = [new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 10,
                    stroke: new ol.style.Stroke({
                        color: '#fff'
                    }),
                    fill: new ol.style.Fill({
                        color: '#3399CC'
                    })
                }),
                text: new ol.style.Text({
                    text: size.toString(),
                    fill: new ol.style.Fill({
                        color: '#fff'
                    })
                })
            })];
            clusterStyleCache_airconclimatedata[size] = style;
        }
        return style;
    }
    var value = ""
    var style = [new ol.style.Style({
        image: new ol.style.Circle({
            radius: 5.0,
            stroke: new ol.style.Stroke({
                color: 'rgba(0,0,0,255)',
                lineDash: null,
                width: 1
            }),
            fill: new ol.style.Fill({
                color: "rgba(251,0,0,1.0)"
            })
        })
    })];
    var labelText = "";
    var key = value + "_" + labelText

    if (!textStyleCache_airconclimatedata[key]) {
        var text = new ol.style.Text({
            font: '26.0px Calibri,sans-serif',
            text: labelText,
            fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
            }),
        });
        textStyleCache_airconclimatedata[key] = new ol.style.Style({
            "text": text
        });
    }
    var allStyles = [textStyleCache_airconclimatedata[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};