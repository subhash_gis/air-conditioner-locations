var ranges_lgaac4 = [
    [1.000000, 3.000000, [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(0,0,0,1.0)",
            lineDash: null,
            width: 0
        }),
        fill: new ol.style.Fill({
            color: "rgba(247,251,255,1.0)"
        })
    })]],
    [3.000000, 7.000000, [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(0,0,0,1.0)",
            lineDash: null,
            width: 0
        }),
        fill: new ol.style.Fill({
            color: "rgba(199,220,239,1.0)"
        })
    })]],
    [7.000000, 13.000000, [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(0,0,0,1.0)",
            lineDash: null,
            width: 0
        }),
        fill: new ol.style.Fill({
            color: "rgba(114,178,215,1.0)"
        })
    })]],
    [13.000000, 27.000000, [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(0,0,0,1.0)",
            lineDash: null,
            width: 0
        }),
        fill: new ol.style.Fill({
            color: "rgba(40,120,184,1.0)"
        })
    })]],
    [27.000000, 53.000000, [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "rgba(0,0,0,1.0)",
            lineDash: null,
            width: 0
        }),
        fill: new ol.style.Fill({
            color: "rgba(8,48,107,1.0)"
        })
    })]]
];
var textStyleCache_lgaac4 = {}
var clusterStyleCache_lgaac4 = {}
var style_lgaac4 = function(feature, resolution) {

    var value = feature.get("NetHERSCnt");
    var style = ranges_lgaac4[0][2];
    for (i = 0; i < ranges_lgaac4.length; i++) {
        var range = ranges_lgaac4[i];
        if (value > range[0] && value <= range[1]) {
            style = range[2];
        }
    };
    var labelText = "";
    var key = value + "_" + labelText

    if (!textStyleCache_lgaac4[key]) {
        var text = new ol.style.Text({
            font: '26.0px Calibri,sans-serif',
            text: labelText,
            fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
            }),
        });
        textStyleCache_lgaac4[key] = new ol.style.Style({
            "text": text
        });
    }
    var allStyles = [textStyleCache_lgaac4[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};