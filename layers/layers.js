var baseLayer = new ol.layer.Group({
    'type': 'base',
    'title': 'Base maps',
    layers: [new ol.layer.Tile({
        type: 'base',
        title: 'MapQuest roads',
        source: new ol.source.MapQuest({
            layer: 'osm'
        })
    })]
});
var lyr_lgaac4 = new ol.layer.Vector({
    source: new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojson_lgaac4)
    }),

    style: style_lgaac4,
    title: "NatHERS Climate Zones"
});
var cluster_airconclimatedata = new ol.source.Cluster({
    distance: 40.0,
    source: new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojson_airconclimatedata)
    }),
});
var lyr_airconclimatedata = new ol.layer.Vector({
    source: cluster_airconclimatedata,
    style: style_airconclimatedata,
    title: "Air-conditioner Location"
});

lyr_lgaac4.setVisible(true);
lyr_airconclimatedata.setVisible(true);
var layersList = [baseLayer, lyr_lgaac4, lyr_airconclimatedata];
var singleLayersList = [lyr_lgaac4, lyr_airconclimatedata];
var selectableLayersList = [lyr_lgaac4, lyr_airconclimatedata];